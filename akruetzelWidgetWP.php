<?php
/*
 * #Plugin Name: akruetzelWidgetWP
 * #Plugin URI: http://www.akrutzel.de/
 * #Description: Widgets um die aktuelle und eine 5 Jahre alte Ausgabe anzuzeigen
 * #Version: 2.2
 * # Update: 2018-12-01
 * #Author: Carsten Hoelbing
 * #Author URI: http://www.hoelbing.net/
 * #License: MIT
 */

// ##################################################################################################
/*
* 2 klassen fuer die einzelenen Widgets (unterscheidung; titeltext und unterschiedliche zeitangeben)
* customer post_type -> fuer jede Ausgabe
* eigenener Shortcode -> es werden in einer liste die letzten ausgaben angezeigt
*/
wp_enqueue_style ( 'AkruetzelWidgetPluginWP', plugins_url() . "/akruetzelwidget/css/akruetzelwidget.css" );

// Die Registrierung unseres Widgets
function AkruetzelAusgabe1_Widget_init() {
  register_widget(akruetzelausgabe1_widget);
	register_widget(akruetzelausgabe2_widget);
}
add_action('widgets_init', 'AkruetzelAusgabe1_Widget_init');

/**
 * ********************************************************************************************************
 *
 * Widget fuer die Sidebar
 *
 * ********************************************************************************************************
 */

class akruetzelausgabe1_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'akruetzelausgabe1_widget',

			// Widget name will appear in UI
			'Akruetzel: die Aktuelle Ausgabe',

			// Widget options
			array(
				'classname'   => 'akruetzelausgabe1_widget', //(string) Class name for the widget's HTML container.
				'description' => 'Zeigt die aktuelle Ausgabe als Widget in der Sidebar an'
			)
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {

		$wpQueryArgs = array(
			'post_type' 	=> 'ausgabe',
			'showposts' 	=> '1',
			'post_count' => 1
		);

		// before and after widget arguments are defined by themes
		echo $args['before_widget'];

		$pq = new WP_Query($wpQueryArgs );
		if( $pq->have_posts() ) {
			$title = 'Akruetzel: die Aktuelle Ausgabe';

			echo '<div class="ausgabe-wrapper">';
			echo ' <div class="ausgabe-header">' . '<span class="ausgabe-title">'. $title . '</span>' .'</div>';

			while( $pq->have_posts() ) : $pq->the_post(); ?>
				<div class="ausgabe-body">

					<a href="<?php echo get_permalink(); ?>" rel="bookmark">

						<?php
						if (has_post_thumbnail()) {
							echo '<div class="ausgabe-thumbnail">';
							the_post_thumbnail(
								// thumbnail image size
								array(150, 212),
								// thumbnail meta infos
								array('class'=> "img-responsive", 'alt' => "Link zum Download/Lesen" )
							);
							echo '</div>';
						}
						?>

						<?php
							echo '<div class="ausgabe-content">';
							the_content();
							echo '</div>';
						?>

					</a>
				</div>

				<?php

			endwhile;
			wp_reset_query();

			echo '</div>'; // end "ausgabe-wrapper"
		}

		// before and after widget arguments are defined by themes
		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {
	}
} // Class widget ends here


class akruetzelausgabe2_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'akruetzelausgabe2_widget',

			// Widget name will appear in UI
			'Akruetzel: die Ausgabe vor 5 Jahren',

			// Widget options
			array(
				'classname'   => 'akruetzelausgabe2_widget', //(string) Class name for the widget's HTML container.
				'description' => 'Zeigt die Ausgabe vor 5 Jahren als Widget in der Sidebar an'
			)
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {

		$year = (date( 'Y' ) - 4) ;

		$wpQueryArgs = array(
			'post_type' 	=> 'ausgabe',
			'showposts' 	=> '1',
			//'meta_key'		=> 'aktuelle_ausgabe',
			'meta_value'	=> 1,
			'post_count' => 1,
			'max_num_pages' => 1,
			'date_query' => array(
				'before' => array(
					'year' => $year
				),
			),
		);

    	// before and after widget arguments are defined by themes
		echo $args['before_widget'];

		$pq = new WP_Query($wpQueryArgs );

		if( $pq->have_posts() ) {
			$title = 'Akrützel vor 5 Jahren...';

			echo '<div class="ausgabe-wrapper">';
			echo ' <div class="ausgabe-header">' . '<span class="ausgabe-title">'. $title . '</span>' .'</div>';

			while( $pq->have_posts() ) : $pq->the_post(); ?>
				<div class="ausgabe-body">

					<a href="<?php echo get_permalink(); ?>" rel="bookmark">

						<?php
						if (has_post_thumbnail()) {
							echo '<div class="ausgabe-thumbnail">';
							the_post_thumbnail(
								// thumbnail image size
								array(150, 212),
								// thumbnail meta infos
								array('class'=> "img-responsive", 'alt' => "Link zum Download/Lesen" )
							);
							echo '</div>';
						}
						?>

						<?php
							echo '<div class="ausgabe-content">';
							the_content();
							echo '</div>';
						?>

					</a>
				</div>

				<?php

			endwhile;
			wp_reset_query();

			echo '</div>'; // end "ausgabe-wrapper"
		}

		// before and after widget arguments are defined by themes
		echo $args['after_widget'];
	}

	// Widget Backend
	public function form( $instance ) {
	}
} // Class widget ends here

/**
 * ********************************************************************************************************
 *
 * xxxx
 *
 * ********************************************************************************************************
 */
// most infos from: http://blog.teamtreehouse.com/create-your-first-wordpress-custom-post-type
// http://pressengers.de/tipps/wordpress-anpassen-custom-post-types-erstellen/
// http://www.elmastudio.de/wordpress-custom-post-types-teil2-selbst-individuelle-inhaltstypen-und-taxonomies-anlegen/

 add_action('init', 'ausgabe_register');

 function ausgabe_register() {

 	$labels = array(
 		'name' => _x('Ausgaben', 'post type general name'),
 		'singular_name' => _x('Ausgaben', 'post type singular name'),
 		'add_new' => _x('Neue hinzufügen', 'portfolio item'),
 		'add_new_item' => __('Neue Ausgabe hinzufügen'),
 		'edit_item' => __('Ausgabe bearbeiten'),
 		'new_item' => __('Neue Ausgabe hinzufügen'),
 		'view_item' => __('Ausgabe anzeigen'),
 		'search_items' => __('Ausgabe suchen'),
 		'not_found' =>  __('Keine Ausgaben gefunden'),
 		'not_found_in_trash' => __('Keine Ausgaben im Papierkorb gefunden'),
 		'parent_item_colon' => 'Master Ausgabe'
 	);

  // rewrite rules
	$rewrite = array(
		'slug' => 'ausgabe'
	);

 	$args = array(
 		'labels' => $labels,
		//should they be shown in the admin UI
 		'public' => true,
		//should we display an admin panel for this custom post type
 		'publicly_queryable' => true,
		//should we display an admin panel for this custom post type
 		'show_ui' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
 		'menu_icon' => 'dashicons-book-alt',
		'menu_position' => 4,
		'can_export' => false,
		'has_archive' => true,
		'exclude_from_search' => false,
		'query_var' => true,
 		'rewrite' => $rewrite,
		//WordPress will treat this as a ‘post’ for read, edit, and delete capabilities
 		'capability_type' => 'post',
 		'hierarchical' => false,
		// tags/schlagwoerter werden in der rechten Sidebar mit angezeigt
		'taxonomies' => array('post_tag'),
		//which items do we want to display on the add/edit post page
 		'supports' => array('title', 'editor', 'thumbnail', 'custom-fields'), //,
    'map_meta_cap' => true,
    // add supports for rest api
    'show_in_rest'       => true,
    'rest_base'          => 'ausgabe',
    'rest_controller_class' => 'WP_REST_Posts_Controller'
 	);

 	register_post_type( 'ausgabe' , $args );
 }

 /**
  * erstelle Shortcode
  * dh. wenn auf einer Seite der tag "[akAusgabeWP counter="10"]" eingefuegt wird,
  * wird an dieser Stelle der Code ausgegeben
  */
 function akAusgabe_shortcode($arg) {
 	return show_lastAusgaben($arg);
 }
 add_shortcode( 'akAusgabeWP', 'akAusgabe_shortcode' );

function show_lastAusgaben($arg) {
		// abfrage: wenn kein Wert uebergeben wurde, dann nehme einen default wert
	if (!isset($arg['counter'])) {
	   $arg['counter'] = 12;
	}
	$counter = $arg['counter'];

  $api_url = "https://www.akruetzel.de/wp-json/wp/v2/ausgabe?per_page=".$counter;
  $data = json_decode(join("", file($api_url)));

  echo '<p><strong>Die letzten '.$counter.' Ausgaben des Akrützel findet Ihr hier:</strong></p>';
  echo '<ul class="ausgabecontent">';

  // ausgabe der einzelnen Ausgaben
  foreach ($data as $result) {
    echo '<li>';
    echo '  <a href="'.$result->link.'" rel="bookmark">';
    echo      $result->title->rendered;
    echo '  </a>';
    echo '</li>';
  }
  echo '</ul>';

  echo '<br/>';

}
